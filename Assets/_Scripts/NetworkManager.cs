﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class NetworkManager : MonoBehaviour {
    [SerializeField]
    Text connectionText;


	// Use this for initialization
	void Start () {
        PhotonNetwork.logLevel = PhotonLogLevel.Full;
        PhotonNetwork.ConnectUsingSettings("0.1");
	}
	
	// Update is called once per frame
	void Update () {
        connectionText.text = PhotonNetwork.connectionStateDetailed.ToString();
	}

    void OnJoinedLobby()
    {
        PhotonNetwork.JoinRandomRoom();
        /*RoomOptions ro = new RoomOptions() { isVisible = true, maxPlayers = 10 }; 
        PhotonNetwork.JoinOrCreateRoom("Mike", ro, TypedLobby.Default);*/
    }
    void OnPhotonRandomJoinFailed()
    {
        Debug.Log("Can't join random room!");
        PhotonNetwork.CreateRoom(null);
    }
    void OnJoinedRoom()
    {
    }
}
